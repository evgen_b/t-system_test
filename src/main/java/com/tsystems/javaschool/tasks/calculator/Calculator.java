package com.tsystems.javaschool.tasks.calculator;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
   /** Быстрое и короткое решение, но не точное, так как вычисляется скриптом
    * public String evaluate(String statement) {
        String b = new String();
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine e = manager.getEngineByName("JavaScript");
            b = e.eval(statement).toString();
        } catch (Exception ex) {
            try (PrintStream stream = (System.out.append("Nan"))) {}
        }
        return b;
    }
}
*/

   public String evaluate(String statement){
       MatchParser result = new MatchParser();
       String res = null;
       double x;
       try {
           x = result.Parse(statement);
          if(Math.floor(x)==x){
              res = Integer.toString((int)x);
          }
          else {
              res = new BigDecimal(x).setScale(3, RoundingMode.UP).toString();
          }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return res;
}
}
package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException, NullPointerException{
        int lastindex =0;
        boolean f = false;
        if(((x.isEmpty()==true)&&(y.isEmpty()==true))||(x.isEmpty()==true)) return f = true;
        if((x.isEmpty()==true)||(y.isEmpty()==true)) throw new IllegalArgumentException();
        if((x == null)||(y == null)) throw new NullPointerException();
        Object[] x1 = x.toArray();
        Object [] y1 = y.toArray();
        for (int i = 0; i<x1.length; i++){
            for (int n = lastindex; n < y1.length; n++){
                if(x1[i].equals(y1[n])){
                    lastindex = n;
                    f=true;
                    break;
                }
                else {
                    f=false;
                    break;
                }
            }
        }
        return f;
    }
}
